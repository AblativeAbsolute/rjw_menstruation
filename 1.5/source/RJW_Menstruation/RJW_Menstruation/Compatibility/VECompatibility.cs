﻿using System.Linq;
using VanillaGenesExpanded;
using Verse;

namespace RJW_Menstruation
{
    public static class VECompatibility
    {
        public static ThingDef VEGeneBloodDef(Pawn pawn)
        {
            if (!ModsConfig.BiotechActive || pawn.genes == null) return null;
            foreach(Gene gene in pawn.genes.GenesListForReading.Where(gene => gene.Active))
            {
                ThingDef bloodDef = gene.def.GetModExtension<GeneExtension>()?.customBloodThingDef;
                if (bloodDef != null) return bloodDef;
            }
            return null;
        }
    }
}
