﻿using RJWSexperienceCum;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;

namespace RJW_Menstruation.Sexperience
{
    public class JobDriver_VaginaWashingWithBucket : JobDriver_CleanSelfWithBucket
    {
        const int excretingTime = 300;//ticks - 120 = 2 real seconds, 3 in-game minutes


        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            return pawn.Reserve(pawn, job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            //this.FailOn(delegate
            //{
            //    return !(Comp.TotalCumPercent > 0.001);
            //});
            yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.ClosestTouch);
            Toil excreting = Toils_General.Wait(excretingTime, TargetIndex.None);//duration of 

            excreting.WithProgressBarToilDelay(TargetIndex.A);
            yield return excreting;
            yield return new Toil()
            {
                initAction = delegate ()
                {
                    bool anyExcreted = false;
                    foreach (HediffComp_Menstruation comp in pawn.GetMenstruationComps())
                        if (comp.TotalCumPercent > 0.001)
                        {
                            CumMixture mixture = comp.MixtureOut(RJWSexperience.VariousDefOf.GatheredCum, 0.5f);
                            float amount = mixture.Volume;
                            if (mixture.ispurecum)
                            {
                                Bucket.AddCum(amount);
                            }
                            else
                            {
                                GatheredCumMixture cummixture = (GatheredCumMixture)ThingMaker.MakeThing(VariousDefOf.GatheredCumMixture);
                                cummixture.InitwithCum(mixture);
                                Bucket.AddCum(amount, cummixture);
                            }
                            anyExcreted = true;
                        }
                    if (!anyExcreted) ReadyForNextToil();
                    if (pawn.GetMenstruationComps().Any(c => c.TotalCumPercent > 0.001)) JumpToToil(excreting);
                }
            };

            Toil cleaning = new Toil
            {
                initAction = CleaningInit,
                tickAction = CleaningTick
            };
            cleaning.AddFinishAction(Finish);
            cleaning.defaultCompleteMode = ToilCompleteMode.Never;
            cleaning.WithProgressBar(TargetIndex.A, () => progress / CleaningTime);

            yield return cleaning;

            //yield return excreting;
            yield break;
        }
    }
}
