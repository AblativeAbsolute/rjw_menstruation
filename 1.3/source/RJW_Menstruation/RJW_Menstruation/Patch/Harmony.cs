﻿using HarmonyLib;
using rjw;
using rjw.Modules.Interactions.Internals.Implementation;
using rjw.Modules.Interactions.Rules.PartKindUsageRules;
using rjw.Modules.Shared.Logs;
using System.Collections.Generic;
using System.Reflection;
using Verse;

namespace RJW_Menstruation
{
    [StaticConstructorOnStartup]
    internal static class First
    {
        static First()
        {
            Harmony har = new Harmony("RJW_Menstruation");
            har.PatchAll(Assembly.GetExecutingAssembly());

            InjectIntoRjwInteractionServices();
        }
        private static void InjectIntoRjwInteractionServices()
        {
            ILog log = LogManager.GetLogger("StaticConstructorOnStartup");

            List<IPartPreferenceRule> partKindUsageRules = Unprivater.GetProtectedValue<List<IPartPreferenceRule>>("_partKindUsageRules", typeof(PartPreferenceDetectorService));
            partKindUsageRules.Add(new Interactions.EstrusPartKindUsageRule());
            log.Message("Added 1 rule to PartPreferenceDetectorService._partKindUsageRules");
        }
    }
}
