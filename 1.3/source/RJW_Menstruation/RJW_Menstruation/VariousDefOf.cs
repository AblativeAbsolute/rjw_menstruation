﻿using RimWorld;
using rjw;
using rjw.Modules.Interactions.Rules.PartKindUsageRules.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RJW_Menstruation
{
    public static class VariousDefOf
    {

        public static readonly ThingDef CumFilth = DefDatabase<ThingDef>.GetNamed("FilthCum");
        public static readonly ThingDef GirlCumFilth = DefDatabase<ThingDef>.GetNamed("FilthGirlCum");
        public static readonly ThingDef Tampon = DefDatabase<ThingDef>.GetNamed("Absorber_Tampon");
        public static readonly ThingDef Tampon_Dirty = DefDatabase<ThingDef>.GetNamed("Absorber_Tampon_Dirty");
        public static readonly ThingDef FilthMixture = DefDatabase<ThingDef>.GetNamed("FilthMixture");
        public static readonly HediffDef RJW_IUD = DefDatabase<HediffDef>.GetNamed("RJW_IUD");
        public static readonly HediffDef Hediff_MenstrualCramp = DefDatabase<HediffDef>.GetNamed("Hediff_MenstrualCramp");
        public static readonly HediffDef Hediff_Estrus = DefDatabase<HediffDef>.GetNamed("Hediff_Estrus");
        public static readonly HediffDef Hediff_Estrus_Concealed = DefDatabase<HediffDef>.GetNamed("Hediff_Estrus_Concealed");
        public static readonly HediffDef Hediff_ASA = DefDatabase<HediffDef>.GetNamed("Hediff_ASA");
        public static readonly StatDef MaxAbsorbable = DefDatabase<StatDef>.GetNamed("MaxAbsorbable");
        public static readonly NeedDef SexNeed = DefDatabase<NeedDef>.GetNamed("Sex");
        public static readonly JobDef VaginaWashing = DefDatabase<JobDef>.GetNamed("VaginaWashing");
        public static readonly JobDef Job_LactateSelf = DefDatabase<JobDef>.GetNamed("LactateSelf");
        public static readonly ThoughtDef LeakingFluids = DefDatabase<ThoughtDef>.GetNamed("LeakingFluids");
        public static readonly ThoughtDef CameInsideF = DefDatabase<ThoughtDef>.GetNamed("CameInsideF");
        public static readonly ThoughtDef CameInsideFLowFert = DefDatabase<ThoughtDef>.GetNamed("CameInsideFLowFert");
        public static readonly ThoughtDef CameInsideFFetish = DefDatabase<ThoughtDef>.GetNamed("CameInsideFFetish");
        public static readonly ThoughtDef CameInsideFFetishSafe = DefDatabase<ThoughtDef>.GetNamed("CameInsideFFetishSafe");
        public static readonly ThoughtDef HaterCameInsideFSafe = DefDatabase<ThoughtDef>.GetNamed("HaterCameInsideFSafe");
        public static readonly ThoughtDef HaterCameInsideF = DefDatabase<ThoughtDef>.GetNamed("HaterCameInsideF");
        public static readonly ThoughtDef HaterCameInsideFEstrus = DefDatabase<ThoughtDef>.GetNamed("HaterCameInsideFEstrus");
        public static readonly ThoughtDef CameInsideM = DefDatabase<ThoughtDef>.GetNamed("CameInsideM");
        public static readonly ThoughtDef HaterCameInsideM = DefDatabase<ThoughtDef>.GetNamed("HaterCameInsideM");
        public static readonly ThoughtDef UnwantedPregnancy = DefDatabase<ThoughtDef>.GetNamed("UnwantedPregnancy");
        public static readonly ThoughtDef UnwantedPregnancyMild = DefDatabase<ThoughtDef>.GetNamed("UnwantedPregnancyMild");
        public static readonly ThoughtDef TookContraceptivePill = DefDatabase<ThoughtDef>.GetNamed("TookContraceptivePill");
        public static readonly ThoughtDef HateTookContraceptivePill = DefDatabase<ThoughtDef>.GetNamed("HateTookContraceptivePill");
        public static readonly CompProperties_Menstruation HumanVaginaCompProperties = (CompProperties_Menstruation)Genital_Helper.average_vagina.comps.FirstOrDefault(x => x is CompProperties_Menstruation);
        public static readonly KeyBindingDef OpenStatusWindowKey = DefDatabase<KeyBindingDef>.GetNamed("OpenStatusWindow");
        public static readonly RecordDef AmountofCreampied = DefDatabase<RecordDef>.GetNamed("AmountofCreampied");
        public static readonly RecordDef AmountofFertilizedEggs = DefDatabase<RecordDef>.GetNamed("AmountofFertilizedEggs");
        public static readonly TaleDef TaleCameInside = DefDatabase<TaleDef>.GetNamed("CameInside");



        private static List<ThingDef> allraces = null;
        private static List<PawnKindDef> allkinds = null;
        private static HashSet<HediffDef> allvaginas = null;
        private static HashSet<HediffDef> allanuses = null;
        private static HashSet<HediffDef> allbreasts = null;

        public static List<ThingDef> AllRaces
        {
            get
            {
                if (allraces != null) return allraces;

                List<ThingDef> allThings = DefDatabase<ThingDef>.AllDefsListForReading;
                allraces = allThings.FindAll(x => x.race != null && x.race.IsFlesh);
                return allraces;
            }
        }
        public static List<PawnKindDef> AllKinds
        {
            get
            {
                if (allkinds != null) return allkinds;

                List<PawnKindDef> allKinds = DefDatabase<PawnKindDef>.AllDefsListForReading;
                allkinds = allKinds.FindAll(x => x.race != null);
                return allkinds;
            }
        }
        public static HashSet<HediffDef> AllVaginas
        {
            get
            {
                if (allvaginas != null) return allvaginas;
                allvaginas = new HashSet<HediffDef>();

                foreach(HediffDef hediffDef in DefDatabase<HediffDef>.AllDefsListForReading)
                {
                    if (hediffDef.comps.NullOrEmpty()) continue;
                    foreach (HediffCompProperties comp in hediffDef.comps)
                    {
                        if (comp.compClass == typeof(HediffComp_Menstruation) || (comp.compClass?.IsSubclassOf(typeof(HediffComp_Menstruation)) ?? false))
                        {
                            allvaginas.Add(hediffDef);
                            break;
                        }
                    }
                }
                
                return allvaginas;
            }
        }
        public static HashSet<HediffDef> AllAnuses
        {
            get
            {
                if (allanuses != null) return allanuses;
                allanuses = new HashSet<HediffDef>();

                foreach (HediffDef hediffDef in DefDatabase<HediffDef>.AllDefsListForReading)
                {
                    if (hediffDef.comps.NullOrEmpty()) continue;
                    foreach (HediffCompProperties comp in hediffDef.comps)
                    {
                        if (comp.compClass == typeof(HediffComp_Anus) || (comp.compClass?.IsSubclassOf(typeof(HediffComp_Anus)) ?? false))
                        {
                            allanuses.Add(hediffDef);
                            break;
                        }
                    }
                }

                return allanuses;
            }
        }
        public static HashSet<HediffDef> AllBreasts
        {
            get
            {
                if (allbreasts != null) return allbreasts;
                allbreasts = new HashSet<HediffDef>();

                foreach(HediffDef hediffDef in DefDatabase<HediffDef>.AllDefsListForReading)
                {
                    if (hediffDef.comps.NullOrEmpty()) continue;
                    foreach(HediffCompProperties comp in hediffDef.comps)
                    {
                        if (comp.compClass == typeof(HediffComp_Breast) || (comp.compClass?.IsSubclassOf(typeof(HediffComp_Breast)) ?? false))
                        {
                            allbreasts.Add(hediffDef);
                            break;
                        }
                    }
                }

                return allbreasts;
            }
        }

        // Defs from Milkable Colonists
        public static readonly HediffDef Hediff_Lactating_Drug = DefDatabase<HediffDef>.GetNamedSilentFail("Lactating_Drug");
        public static readonly HediffDef Hediff_Lactating_Natural = DefDatabase<HediffDef>.GetNamedSilentFail("Lactating_Natural");
        public static readonly HediffDef Hediff_Lactating_Permanent = DefDatabase<HediffDef>.GetNamedSilentFail("Lactating_Permanent");
        public static readonly HediffDef Hediff_Heavy_Lactating_Permanent = DefDatabase<HediffDef>.GetNamedSilentFail("Heavy_Lactating_Permanent");
        public static readonly JobDef Job_LactateSelf_MC = DefDatabase<JobDef>.GetNamedSilentFail("LactateSelf_MC");
    }
}
